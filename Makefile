NAME = PPC
CURSOS = Software Energia Automotiva Eletronica Aeroespacial
CURSOS = Software

BIBTEX = bibtex
LATEX = pdflatex

VERSION = 0.1.0

FIXOS_DIR = fixos
FIXOS_FILES = ${wildcard $(FIXOS_DIR)/*.tex}

COMUNS_DIR = comuns
COMUNS_FILES = ${wildcard $(COMUNS_DIR)/*.tex}

DATA_DIR = dados
DATA_FILES = ${wildcard $(DATA_DIR)/*.csv}

INI_DIR = disciplinas
INI_FILES = ${wildcard $(INI_DIR)/*.txt}

SCRIPTS_DIR = scripts
CVS2TABLE = $(SCRIPTS_DIR)/cvs2table.py
INI2TEX = $(SCRIPTS_DIR)/ini2tex.py
TABLES_DIR = tabelas

INI_SRCS = $(addprefix $(TABLES_DIR)/, $(addsuffix .tex, $(notdir $(basename $(INI_FILES)))))
DATA_SRCS = $(addprefix $(TABLES_DIR)/, $(addsuffix .tex, $(notdir $(basename $(DATA_FILES)))))

.PHONY: all clean dist-clean
.SUFFIXES: .tex .pdf

all: 
	@mkdir -p $(TABLES_DIR)
	@for curso in $(CURSOS); do \
		touch $(NAME)_$$curso.tex; \
		make $(NAME)_$$curso.pdf; \
	done;
#	@make $(TARGET)

.tex.pdf: $(SOURCES) bibliografia.bib
	@python2 $(INI2TEX)
	$(LATEX) $<
#	$(BIBTEX) $(AUX_FILE)
#	$(LATEX) $(MAIN_FILE)

$(TABLES_DIR)/%.tex: $(INI_DIR)/%.txt
	@python2 $(INI2TEX)

$(TABLES_DIR)/%.tex: $(DATA_DIR)/%.csv
	@python2 $(CVS2TABLE)

clean:
	rm -f *~ *.dvi *.ps *.backup *.aux *.log
	rm -f *.lof *.lot *.bbl *.blg *.brf *.toc *.idx
	rm -f *.pdf
	
dist: clean
	tar vczf $(NAME)-$(VERSION).tar.gz *

dist-clean: clean
	rm -f $(PDF_FILE) $(TARGET)
