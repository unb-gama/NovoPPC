with open('portaria_23_2010.tex', 'r') as F:
    data = F.read()

lines = data.splitlines()
L = []
for line in lines:
    if line.startswith('%'):
        L.append(line)
    elif '\\dotfill' in line:
        L.append('%TO-DO dotfill ' + line)
    else:
        L.append(line)
        
with open('portaria_23_2010.tex', 'w') as F:
    F.write('\n'.join(lines))
    
