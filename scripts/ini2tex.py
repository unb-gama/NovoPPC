# !/usr/bin/python
# -*- coding: utf-8 -*-

import glob
import os
import re


def read(datafile):
    with open(datafile) as f:
        data = f.readlines()
        for i, line in enumerate(data):
            for c in '%&#{}':
                line = line.replace('\\' + c, c)
                line = line.replace(c, '\\' + c)
            data[i] = line

    return data


def extract_id(path):
    path = os.path.splitext(path)[0]
    cod = path.rpartition('-')[-1]
    if cod in ['xxxxxx', 'sem_codigo', 'semcodigo']:
        return None
    if not cod.isdigit():
        raise ValueError('invalid code for path: %s' % path)
    return cod

    
def ini2tex(data, cod):
    tex = []
    size = 'scriptsize'

    tex.append('\\begin{%s}' % size)
    tex.append('\\begin{longtable}{p{15cm}}')
    tex.append('\\toprule')
    tex.append('\\rowcolor[gray]{0.9}')

    name = data[0].strip()
    data.pop(0)
    
    print(cod)
    if cod is None:
        cod = ''
    else:
        cod = '$\quad$(%s)' % cod
    
    tex.append('{\\textbf{\\textsc{%s}%s}} \\\\' % (name, cod))
    tex.append('\\midrule')

    for line in data:
        if '[' in line and ']' in line:
            _, line = line.split('[')
            line, _ = line.split(']')

            if 'requisito' in line.lower():
                tex.append('\\midrule')
                tex.append('\\textbf{Pre-Requisito:} ')
            else: 
                tex.append('\\textbf{%s} \\\\' % line)
        else:
            tex.append('{} \\\\'.format(line))

    tex.append('\\bottomrule')
    tex.append('\\end{longtable}')
    tex.append('\\end{%s}' % size)
    tex.append('\n')

    return tex

    for info in data:
        tex.append(rule)
        blocks = info.replace('%','\\%').strip().split('"')
        blocks = info.replace('&','\\&').strip().split('"')
        fields = []

        for block in blocks:
            if i % 2 == 0:
                fields.extend(block.split(','))
            else:
                fields.append(block)

        if first:
            fields = ['\\textbf{%s}' % field for field in fields if len(field) > 0]
        else:
            fields = [field for field in fields if len(field) > 0]

        tex.append('{}\\\\'.format(' & '.join(fields)))
        rule = '\\midrule'
        first = False

    tex.append('\\bottomrule')
    tex.append('\\end{tabularx}')
    tex.append('\\end{table}')

    return tex


def output_filename(datafile):
    # filename = os.path.basename(datafile) # _, filename = datafile.split('/')
    directory, filename = os.path.split(datafile)
    directory = re.sub('/_ini', '', directory)
#    print('directory = %s' % directory)
    name, _ = filename.split('.')

    return '{}/{}/{}.tex'.format(directory, 'tabelas', name)


def save(tex, texfile):
    with open(texfile, 'w') as f:
        f.write('\n'.join(tex))


if __name__ == '__main__':
#	for wildchar in ['_ini/*/', '_ini/']:
	for wildchar in ['disciplinas/*/_ini/', 'disciplinas/_ini/']:
		for folder in glob.glob(wildchar):
#			print 'cur_folder =', folder
			for datafile in glob.glob(folder + '*.txt'):
				print('datafile = %s' % datafile)
				data = read(datafile)
				cod = extract_id(datafile)
				tex = ini2tex(data, cod)
				texfile = output_filename(datafile)
				save(tex, texfile)
