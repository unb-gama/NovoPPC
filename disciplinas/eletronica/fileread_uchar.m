function f = fileread_uchar(file_name)

fp = fopen(file_name,'rb');
f = fread(fp,Inf,'uchar=>uchar')';
fclose(fp);