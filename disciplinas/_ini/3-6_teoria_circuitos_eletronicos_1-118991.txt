Teoria de Circuitos Eletrônicos 1

[Ementa]
Grandezas elétricas.
Conceitos básicos de circuitos elétricos.
Elementos armazenadores de energia.
Leis de Kirchhoff e circuitos resistivos.
Funções singulares: degrau unitário, rampa unitária, impulso.
Métodos de resolução: análise nodal e análise de malhas.
Transformada de Laplace: Análise de transitórios e em regime permanente senoidal.
Técnicas de análise: superposição, transformação de fontes, equivalentes Thevenin e Norton.
Diodo ideal e Amplificadores Operacionais.
Equivalentes Thevenin e Norton.
Circuitos de 1ª e 2ª ordens.

[Programa]
1. Grandezas elétricas
1.1 Tensão elétrica e diferença de potencial;
1.2 Corrente elétrica;
1.3 Potência elétrica e convenção de sinal;
1.4 Noções de impedância e admitância elétrica;
2. Conceitos básicos de circuitos
2.1 Limitações da teoria de circuitos, circuitos lineares e invariantes no tempo;
2.2 Fontes de tensão e de corrente: ideais e reais, independentes ou controladas;
2.3 Lei de Ohm e impedância resistiva; resistores ideais e reais;
2.4 Chaves e interruptores;
2.5 Referência de 0V e caminho de retorno;
3. Leis de Kirchhoff
3.1 Lei de Kirchhoff das Tensões;
3.2 Lei de Kirchhoff das Correntes;
4. Circuitos resistivos
4.1 Associação em paralelo e divisor de corrente;
4.2 Associação em série e divisor de tensão, com ou sem resistor de carga;
4.3 Resistência equivalente. Conversão delta-Y.
4.4 Medições em circuitos: voltímetro, amperímetro e Ponte de Wheatstone;
5. Métodos de resolução
5.1 Análise nodal;
5.2 Análise de malhas;
6. Técnicas de análise
6.1 Teorema da superposição;
6.2 Transformação de fontes;
6.3 Equivalente Thevenin e Equivalente Norton;
7. Elementos armazenadores de energia
7.1 Capacitores e indutores;
7.2 Equações do capacitor/indutor ideal;
7.3 Associações em série e em paralelo;
7.4 Energia acumulada;
7.5 Condições iniciais;
8. Transformada de Laplace em circuitos
8.1 Frequência complexa;
8.2 Definição e propriedades da Transformada de Laplace;
8.3 Funções singulares: degrau, rampa e impulso – definições e principais usos em circuitos;
8.4 Principais pares de transformada de Laplace;
8.5 Leis e teoremas de circuitos no domínio da frequência;
8.6 Impedâncias complexas com ou sem energia acumulada;
8.7 Impedâncias em série e em paralelo;
8.8 Função de transferência;
8.9 Respostas ao degrau e ao impulso;
8.10 Equacionamento de circuitos e resolução por expansão em frações parciais;
8.11 Regime permanente senoidal como caso especial da Transformada de Laplace;
8.12 Fasores e diagramas fasoriais monofásicos;
8.13 Equacionamento e resolução de circuitos por fasores;
9. Diodos
9.1 Diodo ideal e queda de tensão constante;
9.2 Principais topologias: limitadores, grampeadores e ceifadores;
9.3 Circuitos retificadores de meia onda e onda completa;
10. Amplificadores operacionais (Amp Op)
10.1 Definição de amplificador e ganho de potência;
10.2 Ganho de tensão e ganho de corrente;
10.3 Decibel e valor eficaz;
10.4 Saturação e eficiência do amplificador;
10.5 Entradas em modo comum e modo diferencial;
10.6 Impedância de entrada e impedância de saída;
10.7 Amp op ideal - conceitos e principais topologias
10.8 Amp op real - apresentação dos conceitos de offset (tensão e corrente), CMRR, PSRR, slew rate.
10.9 Análise de circuitos com ganho finito, saturação, offset e CMRR.
11. Circuitos de 1ª E 2ª ordens
11.1 Ordem de um circuito;
11.2 Obtenção da equação diferencial de um circuito;
11.3 Tipos de resposta;
11.4 Resposta natural de circuitos RC e RL;
11.5 Constante de tempo;
11.6 Resposta natural de circuitos de 2ª ordem;
11.7 Equação característica;
11.8 Atenuação e amortecimento;
11.9 Frequências (natural e amortecida) de oscilação;
11.10 Respostas forçada e completa de circuitos de 1ª e 2ª ordens;

[Bibliografia Básica]
Richard C. Dorf e James A. Svoboda. Introdução aos Circuitos Elétricos. 8ª edição. Editora: LTC. Ano 2012
Behzad Razavi. Fundamentos de Microeletrônica. 1ª edição. Editora: LTC. Ano 2010
James W. Nilsson e Susan A. Riedel. Circuitos Elétricos. 8ª edição. Editora: Prentice Hall. Ano 2009

[Bibliografia Complementar]
Robert L. Boylestad e Louis Nashelsky. Dispositivos Eletrônicos. 8ª . Editora: Prentice Hall. Ano 2007.
Albert Malvino e David J. Bates. Eletrônica Volume 1. 7ª edição. Editora: McGraw Hill. Ano 2008.
Adel S. Sedra e Kenneth C. Smith. Microeletrônica. 5ª edição. Editora: Prentice Hall. Ano 2007.
Paul Horowitz e Winfield Hill. The Art of Electronics. 2ª edição. Editora: CUP. Ano 1989
Kraig Mitzner. Complete PCB Design using OrCAD Capture and PCB. 1ª edição. Editora: Newnes. Ano 2009.

[pre-requisito]
Equações Diferenciais 1 OU Cálculo 2
