Gerência de Configuração e Evolução de Software

[Ementa]
Identificação de configuração (itens e linha-base)
Controle de mudanças e versões
Integração e entrega contínua
Monitoramento do desenvolvimento de software
Gerenciamento do processo de construção/build (mapeamento para ferramentas para ambientes de desenvolvimento e produção)
Pacotes e dependência de software
Princípios e técnicas de manutenção de software
Sustentação de software

[Programa]

01. Definição de projetos para Manutenção e Evolução de Software
- Introdução à licenças de software
- Desenvolvimento colaborativo e distribuído
- Escolha de projetos aplicação prática dos conteúdos da disciplina

02. Controle de versão
- Tipos e exemplos de sistemas de controle de versão
- Estratégias de commit
- Versões experimentais (branch)
- Estratégias de mesclas (merges)

03. Entrega contínua
- Linha-base
- Integração contínua
- Empacotamento

04. Monitoramento da qualidade interna
- Estratégias de Código limpo
- Padrões de implementação
- Métricas de código-fonte

05. Manutenção e evolução de software
- Manutenção corretiva
- Manutenção preventiva

06. Projeto de Manutenção e Evolução de Software
- Implementação de melhorias em projetos de software (em uso/produção) 
- Gestão e resolução de dívida técnica

[Bibliografia Básica]
GRUBB, Penny; TAKANG, Armstrong A. Software maintenance: concepts and practice. 2nd ed. Hackensack: World Scientific, 2011. xix, 349 p. ISBN 9789812384263.
Kent Beck. Programação Extrema Explicada: escolha as mudanças. Bookman, 2004.
(eBrary) Preibel, René, and Stachmann, Bjorn. Git : Distributed Version Control--Fundamentals and Workflows. Vancouver, CA: Brainy Software, 2014.

[Bibliografia Complementar]
(eBrary) Hongji Yang, Martin Ward. Successful Evolution of Software Systems. Artech House, 2002.
Steve MacConnell. Code Complete. Microsoft Press, 2004.
Ken Schwaber. Agile Project Management with Scrum. Microsoft Press, 2004.
(eBrary) Ewart, John. Chef Essentials. Olton, GB: Packt Publishing, 2014.
(eBrary) Preibel, René, and Stachmann, Bjorn. Git : Distributed Version Control--Fundamentals and Workflows. Vancouver, CA: Brainy Software, 2014.
(eBrary) Uphill, Thomas. Mastering Puppet. Birmingham, GB: Packt Publishing, 2014. ProQuest ebrary. Web. 19 October 2016.
(eBrary) Krafft, M.. Debian System : Concepts and Techniques. San Francisco, US: No Starch Press, Incorporated, 2005. ProQuest ebrary. Web. 19 October 2016. 
Kent Beck. TDD: Desenvolvimento Guiado por Testes. Bookman, 2004.

[Pré-requisito]
Teste de Software.
