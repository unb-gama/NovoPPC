Sistemas de Bancos de Dados 1

[Ementa]
Conceitos de sistemas de banco de dados
Sistema Gerenciador de Banco de Dados
Modelagem de dados: modelagem conceitual e modelo relacional (modelagem lógica)
Banco de dados relacional: restrições de integridade e álgebra relacional
Linguagem SQL (Structured Query Language)
Projeto de banco de dados relacional: dependências funcionais, formas normais e implementação física
Processamento de transações

[Programa]
01. Conceitos Básicos
 - Histórico e componentes de um Banco de Dados
 - Funções de um Sistema de Banco de Dados (SBD)
 - Arquitetura de SBD e independência de dados
 - Sistema Gerenciador de Banco de Dados (SGBD)
 - Componentes do SGBD
02. Modelagem de dados
 - Modelo de Entidade e Relacionamento (ME-R): modelagem conceitual (entidades, atributos e relacionamentos)
 - Diagrama de Entidade e Relacionamentos (DE-R)
 - Modelo Relacional de Dados (MR): modelagem lógica
 - Ferramentas interativas de banco de dados
03. Banco de dados relacional
 - Restrições de integridade
 - Álgebra relacional
 - Mapeamento do ME-R para MR (conceitual para lógico)
04. Normalização
 - Dependência funcional e Forma normal (FN)
 - 1a., 2a., 3a Formas Normais
 - Forma Normal de Boyce-Codd
05. Linguagem SQL (Structured Query Language)
 - Processamento de declarações SQL
 - DDL - Data Definition Language: principais instruções (create, drop, alter) e objetos (table, sequence, view)
 - DML - Data Manipulation Language: principais instruções (insert, update, delete, select)
 - DQL - Data Query Language: principal instrução (select) e suas diversas variações
 - DCL - Data Control Language: principais instruções (grant, revoke) e objetos (user, privilege, role)
 - DTL - Data Transaction Language: principais instruções (commit, rollback)
06. Projeto de banco de dados relacional
 - Elaboração de projeto de banco de dados (níveis conceitual, lógico e físico)
 - Dicionário de dados
07. Processamento de transações
 - Características fundamentais da transação em banco de dados
 - Estados da transação

[Bibliografia Básica]
(eBrary) TEORY, T. LIGHTSTONE, S., NADEAU, T. and JAGADISH, H. V. Database Modeling and Design : Logical Design. USA: Morgan Kaufmann, 2005.
DATE, C. J. Introdução a Sistemas de Bancos de Dados. 5ª. Editora Campus, 2006.
SILBERSCHATZ, A., KORTH, H. F. e SUDARSHAN, S. Sistemas de Bancos de Dados. Editora Campus. 2006.

[Bibliografia Complementar]
ELMASRI, R. E. e NAVATHE, S. Sistemas de Banco de Dados, Editora: PEARSON BRASIL. 2012. ISBN: 857639085X. (01 Livro)
(eBrary) Hutchings, Andrew, and Golubchik, Sergei. MySQL 5.1 Plugins Development : Extend MySQL to Suit Your Needs with this Unique Guide into the World of MySQL Plugins. Olton, Birmingham, GBR: Packt Publishing, 2010.
(eBrary) Davies, Alex. High Availability MySQL Cookbook. Olton, Birmingham, GBR: Packt Publishing, 2010.
(eBrary) Lightstone, Sam, Nadeau, Tom, and Teorey, Toby. Database Modeling and Design : Logical Design. Burlington, MA, USA: Morgan Kaufmann, 2005.
(eBrary) Schneller, Daniel, and Schwedt, Udo. MySQL Admin Cookbook. Olton, Birmingham, GBR: Packt Publishing, 2010.

[Pré-Requisito]
Matemática Discreta 2