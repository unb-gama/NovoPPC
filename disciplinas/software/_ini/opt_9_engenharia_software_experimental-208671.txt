Engenharia de Software Experimental

[Ementa]
Princípios e técnicas para experimentação em Engenharia de Software.
Planejamento e condução de experimentos.
Revisão de processos de medição e técnicas estatísticas para análise de dados.
Análise crítica de artigos científicos recentes.

[Programa]
01. Princípios gerais de metodologia e experimentação
- Paradigmas científicos de produção de conhecimento
- Abordagens, métodos e técnicas
- Planejamento
- Abordagens qualitativas, quantitativas e mistas
- Ferramentas de gerenciamento de referências
- Análise de dados e publicação de trabalhos
02. Experimentação em Engenharia de Software
- Tipos de problemas e questões metodológicas
- O projeto de software como um experimento
- Fontes de incerteza em engenharia de software
- Métricas de software: objetivas, subjetivas, métricas para processo, projeto e produto, métricas de código-fonte (qualidade interna).
- Etnografia em engenharia de software
03. Projeto e definição de pesquisa
- Definição de questão e pesquisa
- Revisão crítica da literatura relacionada
- Proposição de hipótese
- Coleta de dados e definição de variáveis dependentes e independentes.
04. Aplicação de técnicas estatísticas para análise de dados
- Análise fatorial
- Correlações
- Análise multivariada
- Testes de hipóteses

[Bibliografia Básica]
WOHLIN, C., RUNESON, P., HOST, M., OHLSSON, M. C., REGNELL, B., WESSLÉN, a. Experimentation in Software Engineering: An Introduction. Massachusets: Kluwer Academic Publishers, USA. 1ª Edição. 2000.
JURISTO, N.; MORENO, A.M. Basics of Software Engineering Experimentation. Berlim: The Kluwer International Series in Software Engineering. 2000.
TRAVASSOS, G. H.; GUROV, D. E AMARAL, E.A. G. Introdução à Engenharia de Software Experimental. Programa de Engenharia de Sistemas. COPPE. Rio de Janeiro.2002.

[Bibliografia Complementar]
Peter R. Nelson, Karen A.F. Copeland, Marie Coffin, Introductory Statistics for Engineering Experimentation, ISBN 0125154232, 2003.
R. YIN. Estudo de caso: planejamento e métodos. Bookman, 2001.
Barry Boehm, Hans Dieter Rombach, Marvin V. Zelkowitz, Foundations of Empirical Software Engineering: The Legacy of Victor R. Basili, ISBN 3540245472, 2005.
Colin Robinson. Real World Research: A resource for Social Scientists and Practitioner Researchers, ISBN 9780631213055, 2002.
Forrest Shull, Janice Singer, Dag I.K. Sjoberg. Guide to Advanced Empirical Software Engineering, ISBN 184800043X, 2007.
Nigel G Fielding, Raymond M. Lee, Grant Blank. The Handbook of Online Research Methods, ISBN 1412922933, 2008.
Nozer D. Singpurwalla, Simon P. Wilson. Statistical Methods in Software Engineering: Reliability and Risk, ISBN 0387988238, 1999.
J. Zobel. Writing for Computer Science, 2nd edition, Springer, ISBN 1-85233-802-4, 2005.
M. Louis and Richard A. Parker. Designing and Conducting Survey Research - A comprehensive guide, Jossey-Bass, 1997.
Matthew B. Miles, Michael Huberman, Qualitative Data Analysis: An Expanded Sourcebook, ISBN 9780803955400, 1994.

[Pré-requisito]
Probabilidade e Estatística Aplicada à Engenharia E
Qualidade de Software 1.
