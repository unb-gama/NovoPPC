Projeto de Veículos
[Ementa]
1.	Aspectos históricos da engenharia automotiva.
2.	Características de um veículo automotor.
3.	Ferramentas aplicadas ao projeto de veículos automotores
4.	Parâmetros de desempenho para o projeto de veículos automotores.
5.	Sistemas modernos de manufatura na Indústria automotiva.
6.	Atividades relacionadas ao projeto de um veículo automotor.

[Programa]
1.	Aspectos históricos da engenharia automotiva.
2.	Características de um veículo automotor.
3.	Ferramentas aplicadas ao projeto de veículos automotores
4.	Parâmetros de desempenho para o projeto de veículos automotores.
5.	Sistemas modernos de manufatura na Indústria automotiva.
6.	Atividades relacionadas ao projeto de um veículo automotor.


[Bibliografia Básica]
HAPPIAN-SMITH, Julian (Ed.). An introduction to modern vehicle design. SAE International, c2002. ISBN 0768005965.
BRAESS, Hans-Hermann; SEIFFERT, Ulrich (Ed.). Handbook of automotive engineering. SAE International, c2005 ISBN 9780768007831.
MILLIKEN, William F.; OLLEY, Maurice; MILLIKEN, Douglas L. Chassis design: principles and analysis. SAE International, 2002. ISBN 0768008263


[Bibliografia Complementar]
MACEY, Stuart; WARDLE, George. H-point: the fundamentals of car design \& packaging. Pasadena: Art Center College of Design, c2008. ISBN 9781933492377.
Hucho, Wolf-Heinrich, Aerodynamics of road vehicles : from fluid mechanics to vehicle engineering. Butterworth-Heinemann.1987. ISBN  0408014229
VIEIRA, José Luiz. A história do automóvel: a evolução da mobilidade, ISBN 9788598497860.
GILLESPIE, T. D. Fundamentals of vehicle dynamics. Warrendale: SAE International, 1992 ISBN 1560911999.
NICOLAZZI.  Fundamentos da engenharia automotiva(apostila) /USFC-BRA]

[Pré-requisito]
Projeto de Estruturas de Veículos
