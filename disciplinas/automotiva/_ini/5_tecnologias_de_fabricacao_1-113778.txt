Tecnologias de Fabricação 1
[Ementa]
1. Tolerâncias, Ajustes e Acabamento Superficial;
2. Fundamentos de Usinagem dos Metais;
3. Fundamentos de Conformação Mecânica;
[Programa]
1. Tolerâncias, Ajustes e Acabamento Superficial
1.1 Conceitos Básicos;
1.2 Sistemas de tolerância e ajustes;
1.3 Tolerância dimensional;
1.4 Tolerância geométrica;
1.5 Acabamento superficial;
2. Fundamentos da Usinagem dos Metais
2.1 Introdução a Usinagem;
2.2 Nomenclatura e Geometria das Ferramentas de Corte;
2.3 Grandezas Físicas no Processo de Corte;
2.4 Formação e Controle do Cavaco;
2.5 Força e Potência de Usinagem;
2.6 Temperatura e Fluidos de Corte;
2.7 Materiais para Ferramentas de Corte;
2.8 Desgaste e Vida das Ferramentas de Corte;
2.9 Retificação;
2.10 Usinagem por Descargas Elétricas;
2.11 Comando Numérico Computadorizado.
3. Fundamentos da Conformação dos Materiais
3.1 Trefilação;
3.2 Extrusão;
3.3 Forjamento;
3.4 Estampagem;
[Bibliografia Básica]
CHIAVERINI, V., "Tecnologia Mecânica", Vol. 2, 2ª Edição, Ed. McGraw-Hill, 315 pg., 1986.
MACHADO, A. R.; COELHO, R. T.; ABRÃO, A. M.; SILVA, M. B., "Teoria da Usinagem dos Materiais", 2ª Edição Revista, Ed. Edgard Blucher, 400 pg., 2011.
FERRARESI, D., "Fundamentos da Usinagem dos Metais", Ed. Edgard Blucher, 800 pg., 1977.
[Bibliografia Complementar]
TRENT, E. M.; WRIGHT, P. K., "Metal Cutting", 4ª Edição, Ed. Butterworth-Heinemann, 446 pg., 2000.
SCHAEFFER, L. Conformação de Chapas Metálicas, 2ª Edição, Ed. Imprensa Livre, 150 pg., 2004.
SCHAEFFER, L. Forjamento - Introdução ao Processo, 2ª Edição, Ed. Imprensa Livre, 202 pg., 2006.
BRITO, O. Estampos de Formar, 2ª Edição, Ed. Hemus, 220 pg., 2005.
BRITO, O. Estampos de Corte, 2ª Edição, Ed. Hemus, 185 pg., 2004.
[Pré-requisito]
Materiais de Construção para Engenharia
